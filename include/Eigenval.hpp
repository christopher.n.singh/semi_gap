///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Eigenval.hpp ***                             //
//                                                                           //
// Base class responsible for reading in and storing the information         //
// contained within the VASP output file EIGENVAL                            //
//                                                                           //
// created November 09, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Eigenval_hpp
#define Eigenval_hpp

#include <string>
#include <sstream>
#include <fstream>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <algorithm>

class Eigenval
{
	public:

		Eigenval(std::string file);
		void print(void);
        void get_stream_line(std::ifstream &data, std::string &line,
							 std::istringstream &stream_line);

		double occupation(int kpoint, int band);
		double energy(int kpoint, int band);
		double conduction_band_minimum(int kpoint);
		double valence_band_maximum(int kpoint);
		double gap(int k1, int k2);
	 	void calculate_momentum_dependent_gap(void);
		void print_momentum_dependent_gap(void);
		void write_momentum_dependent_gap(std::string file);
		double smallest_gap(void);
		double largest_gap(void);
		void find_largest_gap_k1k2(void);
		void find_smallest_gap_k1k2(void);	
		void print_gap_type(void);
		
		int smallest_gap_k1;
		int smallest_gap_k2;

		int largest_gap_k1;
		int largest_gap_k2;


	private:

		int ions;
		int pair_correlation_loops;
		int ispin;
		
		int electrons;
		int kpoints;
		int bands;	


		double volume_of_cell;
		double a_lattice_param;
		double b_lattice_param;
		double c_lattice_param;
		double yet_unknown_parameter;

		double temperature;

		std::string car;
		std::string header;

		double *kvecs;
		double *value;
		double *gaps;
};

















#endif /* Eigenval_hpp */
