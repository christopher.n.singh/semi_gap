#include <iostream>
#include "Eigenval.hpp"

inline bool exists(std::string name) {
	std::ifstream file(name.c_str());
    return file.good();
}

int main(int argc, char *argv[]) 
{

	if (argc == 1 && !exists("EIGENVAL")) {
		
		std::cout << "\n";
		std::cout << " Seems like I cannot find the EIGENVALUE file!\n";
		std::cout << " In this case, you can pass the name yourself\n";
		std::cout << "\n";
		std::cout << " USAGE: gap <MY_EIGENVAL_FILE> <OPTIONAL_OUTPUT_NAME>\n";
		std::cout << std::endl;
		exit(-1);

	} else if (argc == 1 && exists("EIGENVAL")) {
		
		std::string input_file = "EIGENVAL";
		std::string output_file = "GAPCAR";

		Eigenval eigenval(input_file);
		eigenval.write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << eigenval.smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << eigenval.largest_gap() << " (eV)\n";
		std::cout << "\n";
	
	} else if (argc == 2) {
	
		std::string input_file = argv[1];
		std::string output_file = "GAPCAR";

		Eigenval eigenval(input_file);
		eigenval.write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << eigenval.smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << eigenval.largest_gap() << " (eV)\n";
		std::cout << "\n";
	
	} else if (argc == 3) {
		
		std::string input_file = argv[1];
		std::string output_file = argv[2];

		Eigenval eigenval(input_file);
		eigenval.write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << eigenval.smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << eigenval.largest_gap() << " (eV)\n";
		std::cout << "\n";
		
	} else {
		std::cout << "I am not designed to handle any that\n";
		std::cout << " USAGE: mgap.x <MY_EIGENVAL_FILE> <OPTIONAL_OUTPUT_NAME>\n";
		exit(-1);
	}


	return 0;
}
