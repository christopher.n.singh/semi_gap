///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Eigenval.cpp ***                             //
//                                                                           //
// Base class responsible for reading in and storing the information         //
// contained within the VASP output file EIGENVAL                            //
//                                                                           //
// created November 09, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "Eigenval.hpp"

void Eigenval::print_gap_type(void)
{
	if (smallest_gap_k1 == smallest_gap_k2) 
		std::cout << " Direct gap system detected\n";
	else 
		std::cout << " Indiredct gap system detected\n";
}

void Eigenval::find_largest_gap_k1k2(void)
{
	double largest = gaps[0];
	largest_gap_k1 = 0;
	largest_gap_k2 = 0;

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 1; k2 < kpoints; ++k2) {
			if (gaps[k1 * kpoints + k2] > largest) {
				largest = gaps[k1 * kpoints + k2];
				largest_gap_k1 = k1;
				largest_gap_k2 = k2;
			}
		}
	}
}

void Eigenval::find_smallest_gap_k1k2(void)
{
	double smallest = gaps[0];
	smallest_gap_k1 = 0;
	smallest_gap_k2 = 0;

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 1; k2 < kpoints; ++k2) {
			if (gaps[k1 * kpoints + k2] < smallest) {
				smallest = gaps[k1 * kpoints + k2];
				smallest_gap_k1 = k1;
				smallest_gap_k2 = k2;
			}
		}
	}
}


double Eigenval::largest_gap(void)
{
	double *sg;

	sg = std::max_element(gaps, gaps + kpoints*kpoints);

	return *sg;
}

double Eigenval::smallest_gap(void)
{
	double *sg;

	sg = std::min_element(gaps, gaps + kpoints*kpoints);

	return *sg;
}

void Eigenval::write_momentum_dependent_gap(std::string file)
{
	std::ofstream gap_data_stream(file.c_str());

	assert(gap_data_stream.is_open());

	gap_data_stream << "# " << header << "\n";
	gap_data_stream << "#   k1      k2       Eg (eV)\n";

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 0; k2 < kpoints; ++k2) {
			gap_data_stream << std::setw(5);
			gap_data_stream << k1 << "\t";
			gap_data_stream << std::setw(5);
			gap_data_stream << k2 << "\t";
			gap_data_stream << std::setw(15);
			gap_data_stream << std::scientific;
			gap_data_stream << gaps[k1 * kpoints + k2];
			gap_data_stream << "\n";
		}
	}
	
}

void Eigenval::print_momentum_dependent_gap(void)
{
	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 0; k2 < kpoints; ++k2) {
			std::cout << k1 << "\t";
			std::cout << k2 << "\t";
			std::cout << gaps[k1 * kpoints + k2];
			std::cout << "\n";
		}
	}
}

void Eigenval::calculate_momentum_dependent_gap(void)
{
	gaps = new double[kpoints*kpoints];

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 0; k2 < kpoints; ++k2) 
			gaps[k1 * kpoints + k2] = gap(k1, k2);
	}
}

double Eigenval::gap(int k1, int k2) 
{
	return conduction_band_minimum(k1) - valence_band_maximum(k2);
}

double Eigenval::energy(int kpoint, int band)
{
	return value[band*3 + 1 + kpoint*bands*3];
}

double Eigenval::occupation(int kpoint, int band)
{
	return value[band*3 + 2 + kpoint*bands*3];	
}

double Eigenval::conduction_band_minimum(int kpoint)
{
	for (int band = 0; band < bands; ++band) 
		if (occupation(kpoint, band) == 0) return energy(kpoint, band);

	std::cout << " Count not find the bottom of the conduction band!\n";
	exit(-100);
}

double Eigenval::valence_band_maximum(int kpoint)
{
	for (int band = 0; band < bands; ++band) 
		if (occupation(kpoint, band) == 0) return energy(kpoint, band - 1);

	std::cout << " Count not find the top of the valence band!\n";
	exit(-200);
}

void Eigenval::get_stream_line(std::ifstream &data, std::string &line,
							   std::istringstream &stream_line) 
{
	std::getline(data, line);
	stream_line.str(std::string());
	stream_line.clear();
	stream_line.str(line);

}

Eigenval::Eigenval(std::string file)
{
	std::ifstream data(file.c_str());
	assert(data.is_open());

	std::string line;
	std::istringstream stream_line;

	get_stream_line(data, line, stream_line);
	stream_line >> ions;
	stream_line >> ions;
	stream_line >> pair_correlation_loops;
	stream_line >> ispin;

	get_stream_line(data, line, stream_line);
	stream_line >> volume_of_cell;
	stream_line >> a_lattice_param;
	stream_line >> b_lattice_param;
	stream_line >> c_lattice_param;
	stream_line >> yet_unknown_parameter;
	
	get_stream_line(data, line, stream_line);
	stream_line >> temperature;

	std::getline(data, car);
	std::getline(data, header);

	get_stream_line(data, line, stream_line);
	stream_line >> electrons;
	stream_line >> kpoints;
	stream_line >> bands;

	std::getline(data, line);

	kvecs = new double[kpoints*4];
	value = new double[kpoints*bands*3];

	for (int kpoint = 0; kpoint < kpoints; ++kpoint) {
		
		get_stream_line(data, line, stream_line);

		for (int i = 0; i < 4; ++i) stream_line >> kvecs[kpoint * 4 + i];

		for (int band = 0; band < bands; ++band) {
			get_stream_line(data, line, stream_line);
			for (int i = 0; i < 3; ++i) {
				stream_line >> value[band*3 + i + kpoint*bands*3];
			}
		}
		std::getline(data, line);
	}

	calculate_momentum_dependent_gap();
	find_largest_gap_k1k2();
	find_smallest_gap_k1k2();
}

void Eigenval::print(void) 
{
			
	std::cout << ions << ' ' << ions << ' ';
	std::cout << pair_correlation_loops << ' ';
	std::cout << ispin << '\n';

	std::cout << volume_of_cell << ' ';
	std::cout << a_lattice_param << ' ';
	std::cout << b_lattice_param << ' ';
	std::cout << c_lattice_param << ' ';
	std::cout << yet_unknown_parameter << '\n';

	std::cout << temperature << '\n';

	std::cout << car << '\n';
	std::cout << header << '\n';

	std::cout << electrons << ' ';
	std::cout << kpoints << ' ';
	std::cout << bands << '\n';

	std::cout << std::setw(10);
	for (int kpoint = 0; kpoint < kpoints; ++kpoint) {
		for (int i = 0; i < 4; ++i) {
			std::cout << kvecs[kpoint * 4 + i] << "   ";
		}
		std::cout << '\n';
		for (int band = 0; band < bands; ++band) {
			for (int j = 0; j < 3; ++j) {
				std::cout << std::setw(10);
				std::cout << value[band * 3 + j + kpoint*bands*3] << "  ";
			}
			std::cout << "\n";
		}
	}

}
