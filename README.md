# Semi_Gap

Simple C++ program that will calculate the electronic gap as a function of 
momentum from VASP output.

This program utilizes the EIGENVAL file to produce Eg(k1,k2), where k1 and k2 
run through all available kpoints in the file. 

The momentum dependence of the gap is important in semi-conductor physics. If 
you found this program, you probably don't need a description of why you would 
want to know that information.

If the smallest value of the gap occurs when k1 = k2, the system is a direct 
gap material. On the other hand, if the smallest value of Eg occurs when 
k1 != k2, then you have got yourself an indirect gap system.

For an accurate calculation of the gap size, the proper k-mesh is required 
based on you system. Aka, you better have included all the high symmetry 
points, as well as used a fine enough sampling of momentum space or the value 
you get here will be wrong. Not my fault.


# Usage

This has been checked to compile with g++ on OSX 10.13.6 and CentOS 7.4.1708,
and with icpc on CentOS 7.4.1708.

To use it, clone it and build it. Something like this...
 
- git clone https://gitlab.com/christopher.n.singh/semi_gap.git
- cd semi_gap
- make
- mv bin/gap /some/VASP/directory/
- cd /some/VASP/directory/ && gap

The program will first look to find EIGENVAL in your current working directory.
If it cannot find it, you may pass the location of EIGENVAL. You may also 
optionally pass the name of the output file. If no output file is specified, the
data will be written to GAPCAR.

Also the program will print some information like the smallest gap in your 
spectrum and the largest gap in your spectrum.
